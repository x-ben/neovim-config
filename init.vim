""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            BENS NEOVIM CONFIG                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"   CONTENTS
" ------------------
"  Basic Settings
"  Status Line
"  Plug
"  General Settings
"  Ctrl P
"  General Mappings
"  Framework Config
" ------------------

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               BASIC SETTINGS                                 "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible
filetype off
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set encoding=utf-8
set background=dark
colorscheme onedark

" Map leader
let mapleader = ","
let g:mapleader = ","

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  NETRW                                       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

noremap <C-b> :Lexplore<cr>
let g:netrw_liststyle=3         " thin (change to 3 for tree)
let g:netrw_banner=0            " no banner
let g:netrw_winsize=25          " sets the window size

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               STATUSLINE                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set statusline=--\ %F\ --\                            "tail of the filename
set statusline+=%h                                    "help file flag
set statusline+=%m                                    "modified flag
set statusline+=%r                                    "read only flag
set statusline+=%y                                    "filetype
set statusline+=%=                                    "left/right separator
set statusline+=[%l/%L]\                              "cursor line/total lines
set statusline+=--\ %P\ --                            "percent through file

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                         PLUG PACKAGE MANAGER                                 "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin()
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'Shougo/deoplete.nvim'
  Plug 'ervandew/supertab'
  Plug 'tpope/vim-fugitive'
  Plug 'jiangmiao/auto-pairs'
  Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
  Plug 'tpope/vim-repeat'
  Plug 'tpope/vim-endwise'
  Plug 'elixir-lang/vim-elixir'
  Plug 'tpope/vim-rails',          { 'for': 'ruby' }
  Plug 'vim-ruby/vim-ruby',        { 'for': 'ruby' }
  Plug 'thoughtbot/vim-rspec',     { 'for': 'ruby' }
  Plug 'pangloss/vim-javascript',  { 'for': 'javascript' }
  Plug 'kchmck/vim-coffee-script', { 'for': 'coffeescript' }
  Plug 'cakebaker/scss-syntax.vim'
  Plug 'xsbeats/vim-blade',        { 'for': 'php' }
  Plug 'elzr/vim-json'
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             GENERAL SETTINGS                                 "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nu                                 " line numbers
set autoindent                         " always set autoindenting on
set smartindent                        " does the right thing
set copyindent                         " copy the previous indentation
set tabstop=2                          " a tab is 2 spaces
set shiftwidth=2                       " number of spaces to use autoindenting
set softtabstop=2                      " hit Tab in insert mode
set shiftround                         " multiple of shiftwidth when indenting
set expandtab                          " turns tabs to spaces
set scrolloff=5                        " visible lines above and below on serch
set ignorecase                         " ignore case when searching
set smartcase                          " ignore case if search is all lowercase
set hlsearch                           " highlight search text
set showmatch                          " show match brackets
set visualbell                         " don't beep
set noerrorbells                       " don't beep
set autowrite                          " save on buffer switch
set wrap                               " don't wrap lines
set linebreak                          " wrap full words
set tw=80                              " set editor wrap
set colorcolumn=+1                     " Sets charcater guide
set laststatus=2                       " permanently see statusline
set cursorline                         " setting the cursorline
set nobackup                           " no Backup
set nowritebackup                      " no backup file while editing
set noswapfile                         " do not create swap
set backspace=indent,eol,start         " make backspace work as delete
autocmd BufWritePre * :%s/\s\+$//e     " auto-remove trailing spaces
command! H let @/=""                   " remove search results
autocmd VimResized * :wincmd =         " automatically rebalance windows resize
filetype plugin indent on              " plugin indent settings for filetype
syntax enable

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 SUPERTAB                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"let g:SuperTabDefaultCompletionType ="<C-X><C-O>"
"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              DEOCOMPLETE                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:deoplete#enable_at_startup = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                ULTISNIPS                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  CTRL P                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:ctrlp_map = '<c-p>'
noremap <c-e> :CtrlPMRU<cr>
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/vendor/**
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             CUSTOM MAPPINGS                                  "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Fast Saves
nmap <leader>w :w!<cr>
" Down is really the next line
nnoremap j gj
nnoremap k gk
" Easy escaping to normal model
imap jj <esc>
" Set Paste
nnoremap <leader>sp :set paste<cr>
nnoremap <leader>snp :set nopaste<cr>
" New buffer
nnoremap <leader>n :enew<cr>
" New empty tab
noremap <leader>t :tabe
" Move line down by 1 line
nnoremap - ddp
" Move line up by 1 line
nnoremap _ kddpk
" Insert into wtih indents
nnoremap <leader><cr> kA<cr>
" Open a vertical spilt
nnoremap <leader>v :vsplit<cr>
" Open previous file
nnoremap <leader>p :e #<2<cr>
" Go to first character on the line
nnoremap H ^
" Go to last character on the line
nnoremap L $
" Disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           FRAMEWORK CONFIG                                   "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Laravel
autocmd BufRead,BufNewFile *.blade.php set filetype=html
autocmd BufRead,BufNewFile *.blade.php set syntax=blade
