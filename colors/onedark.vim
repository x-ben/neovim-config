set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "onedark"

if has("gui_running") || &t_Co == 88 || &t_Co == 256
  let s:low_color = 0
else
  let s:low_color = 1
endif

" Color approximation functions by Henry So, Jr. and David Liang {{{
" Added to onedark.vim by Daniel Herbert

" returns an approximate grey index for the given grey level
fun! s:grey_number(x)
  if &t_Co == 88
    if a:x < 23
      return 0
    elseif a:x < 69
      return 1
    elseif a:x < 103
      return 2
    elseif a:x < 127
      return 3
    elseif a:x < 150
      return 4
    elseif a:x < 173
      return 5
    elseif a:x < 196
      return 6
    elseif a:x < 219
      return 7
    elseif a:x < 243
      return 8
    else
      return 9
    endif
  else
    if a:x < 14
      return 0
    else
      let l:n = (a:x - 8) / 10
      let l:m = (a:x - 8) % 10
      if l:m < 5
        return l:n
      else
        return l:n + 1
      endif
    endif
  endif
endfun

" returns the actual grey level represented by the grey index
fun! s:grey_level(n)
  if &t_Co == 88
    if a:n == 0
      return 0
    elseif a:n == 1
      return 46
    elseif a:n == 2
      return 92
    elseif a:n == 3
      return 115
    elseif a:n == 4
      return 139
    elseif a:n == 5
      return 162
    elseif a:n == 6
      return 185
    elseif a:n == 7
      return 208
    elseif a:n == 8
      return 231
    else
      return 255
    endif
  else
    if a:n == 0
      return 0
    else
      return 8 + (a:n * 10)
    endif
  endif
endfun

" returns the palette index for the given grey index
fun! s:grey_color(n)
  if &t_Co == 88
    if a:n == 0
      return 16
    elseif a:n == 9
      return 79
    else
      return 79 + a:n
    endif
  else
    if a:n == 0
      return 16
    elseif a:n == 25
      return 231
    else
      return 231 + a:n
    endif
  endif
endfun

" returns an approximate color index for the given color level
fun! s:rgb_number(x)
  if &t_Co == 88
    if a:x < 69
      return 0
    elseif a:x < 172
      return 1
    elseif a:x < 230
      return 2
    else
      return 3
    endif
  else
    if a:x < 75
      return 0
    else
      let l:n = (a:x - 55) / 40
      let l:m = (a:x - 55) % 40
      if l:m < 20
        return l:n
      else
        return l:n + 1
      endif
    endif
  endif
endfun

" returns the actual color level for the given color index
fun! s:rgb_level(n)
  if &t_Co == 88
    if a:n == 0
      return 0
    elseif a:n == 1
      return 139
    elseif a:n == 2
      return 205
    else
      return 255
    endif
  else
    if a:n == 0
      return 0
    else
      return 55 + (a:n * 40)
    endif
  endif
endfun

" returns the palette index for the given R/G/B color indices
fun! s:rgb_color(x, y, z)
  if &t_Co == 88
    return 16 + (a:x * 16) + (a:y * 4) + a:z
  else
    return 16 + (a:x * 36) + (a:y * 6) + a:z
  endif
endfun

" returns the palette index to approximate the given R/G/B color levels
fun! s:color(r, g, b)
  " get the closest grey
  let l:gx = s:grey_number(a:r)
  let l:gy = s:grey_number(a:g)
  let l:gz = s:grey_number(a:b)

  " get the closest color
  let l:x = s:rgb_number(a:r)
  let l:y = s:rgb_number(a:g)
  let l:z = s:rgb_number(a:b)

  if l:gx == l:gy && l:gy == l:gz
    " there are two possibilities
    let l:dgr = s:grey_level(l:gx) - a:r
    let l:dgg = s:grey_level(l:gy) - a:g
    let l:dgb = s:grey_level(l:gz) - a:b
    let l:dgrey = (l:dgr * l:dgr) + (l:dgg * l:dgg) + (l:dgb * l:dgb)
    let l:dr = s:rgb_level(l:gx) - a:r
    let l:dg = s:rgb_level(l:gy) - a:g
    let l:db = s:rgb_level(l:gz) - a:b
    let l:drgb = (l:dr * l:dr) + (l:dg * l:dg) + (l:db * l:db)
    if l:dgrey < l:drgb
      " use the grey
      return s:grey_color(l:gx)
    else
      " use the color
      return s:rgb_color(l:x, l:y, l:z)
    endif
  else
    " only one possibility
    return s:rgb_color(l:x, l:y, l:z)
  endif
endfun

" returns the palette index to approximate the 'rrggbb' hex string
fun! s:rgb(rgb)
  let l:r = ("0x" . strpart(a:rgb, 0, 2)) + 0
  let l:g = ("0x" . strpart(a:rgb, 2, 2)) + 0
  let l:b = ("0x" . strpart(a:rgb, 4, 2)) + 0
  return s:color(l:r, l:g, l:b)
endfun

" sets the highlighting for the given group
fun! s:X(group, fg, bg, attr, lcfg, lcbg)
  if s:low_color
    let l:fge = empty(a:lcfg)
    let l:bge = empty(a:lcbg)

    if !l:fge && !l:bge
      exec "hi ".a:group." ctermfg=".a:lcfg." ctermbg=".a:lcbg
    elseif !l:fge && l:bge
      exec "hi ".a:group." ctermfg=".a:lcfg." ctermbg=NONE"
    elseif l:fge && !l:bge
      exec "hi ".a:group." ctermfg=NONE ctermbg=".a:lcbg
    endif
  else
    let l:fge = empty(a:fg)
    let l:bge = empty(a:bg)

    if !l:fge && !l:bge
      exec "hi ".a:group." guifg=#".a:fg." guibg=#".a:bg." ctermfg=".s:rgb(a:fg)." ctermbg=".s:rgb(a:bg)
    elseif !l:fge && l:bge
      exec "hi ".a:group." guifg=#".a:fg." guibg=NONE ctermfg=".s:rgb(a:fg)." ctermbg=NONE"
    elseif l:fge && !l:bge
      exec "hi ".a:group." guifg=NONE guibg=#".a:bg." ctermfg=NONE ctermbg=".s:rgb(a:bg)
    endif
  endif

  if a:attr == ""
    exec "hi ".a:group." gui=none cterm=none"
  else
    let l:noitalic = join(filter(split(a:attr, ","), "v:val !=? 'italic'"), ",")
    if empty(l:noitalic)
      let l:noitalic = "none"
    endif
    exec "hi ".a:group." gui=".a:attr." cterm=".l:noitalic
  endif
endfun
" }}}

let darkest_blue = "21252B"
let dark_blue = "282c35"
let lighter_blue = "2C323C"
let mid_blue = "3e4352"
let atom_blue = "4f87ff"
let blue_grey = "4B5263"
let silver = "9da4b5"
let white = "FFFFFF"
let purple = "C678DD"
let teal = "56B6C2"
let red = "E06C75"
let yellow = "DEBB79"
let blue = "5EAAE8"
let green = "98C379"
let amber = "D19A66"
let light_grey = "5C6370"

let font_color = g:silver
let background_color = g:dark_blue
let selection = g:mid_blue
let highlight = g:lighter_blue

call s:X("Normal",font_color,g:background_color,"","","")
set background=dark

if !exists("g:onedark_use_lowcolor_black") || g:onedark_use_lowcolor_black
    let s:termBlack = "Black"
else
    let s:termBlack = "Grey"
endif

if version >= 700
  call s:X("CursorLine","",g:highlight,"","",s:termBlack)
  call s:X("CursorColumn","","","","",s:termBlack)
  call s:X("MatchParen",g:atom_blue,"","bold","","Darkteal")

  call s:X("TabLine",g:blue_grey,g:darkest_blue,"italic","",s:termBlack)

  call s:X("TabLineFill",g:highlight,g:darkest_blue,"","",s:termBlack)
  call s:X("TabLineSel",g:atom_blue,g:background_color,"italic,bold",s:termBlack,"White")

  " Auto-completion
  call s:X("Pmenu",g:highlight,g:silver,"","White",s:termBlack)
  call s:X("PmenuSel",g:silver,g:selection,"",s:termBlack,"White")
endif

call s:X("Visual","",g:selection,"","",s:termBlack)
call s:X("Cursor",g:background_color,g:font_color,"","","")

call s:X("LineNr",g:blue_grey,g:background_color,"none",s:termBlack,"")
call s:X("CursorLineNr",g:blue_grey,g:highlight,"none","White","")
call s:X("Comment",g:blue_grey,"","italic","Grey","")
call s:X("Todo",g:teal,"","bold","White",s:termBlack)

call s:X("StatusLine",g:dark_blue,g:silver,"italic","","White")
call s:X("StatusLineNC",g:blue_grey,g:darkest_blue,"italic","White","Black")
call s:X("VertSplit",g:highlight,g:highlight,"",s:termBlack,s:termBlack)
call s:X("WildMenu","f0a0c0","302028","","Magenta","")

call s:X("Folded","a0a8b0","384048","italic",s:termBlack,"")
call s:X("FoldColumn","535D66","1f1f1f","","",s:termBlack)
call s:X("SignColumn",g:dark_blue,g:dark_blue,"","",s:termBlack)
call s:X("ColorColumn","",g:highlight,"","",s:termBlack)

call s:X("Title",g:silver,"","bold","Green","")

call s:X("Constant",g:teal,"","","Red","")
call s:X("Special",g:red,"","","Green","")
call s:X("Delimiter",g:light_grey,"","","Grey","")
call s:X("Define", g:purple, "", "bold", "", "")

call s:X("String",g:green,"","italic","Green","")
call s:X("StringDelimiter",g:light_grey,"","","DarkGreen","")

call s:X("Identifier",g:teal,"","","Lightteal","")
call s:X("Structure",g:blue,"","","Lightteal","")
call s:X("Function",g:silver,"","","Yellow","")
call s:X("Statement",g:purple,"","","Violet","")
call s:X("PreProc",g:blue,"","","LightBlue","")
call s:X("Boolean", g:red, "", "", "", "")
call s:X("Keyword", g:purple, "", "bold", "", "")
call s:X("Typedef",g:yellow,"","","DarkYellow","")

hi! link Operator Structure

call s:X("Type",g:amber,"","","Yellow","")
call s:X("NonText",g:background_color,g:background_color,"",s:termBlack,"")

call s:X("SpecialKey",g:red,g:background_color,"",s:termBlack,"")

call s:X("Search","","302028","underline","Magenta","")

call s:X("Directory",g:silver,"","","Yellow","")
call s:X("ErrorMsg",g:dark_blue,g:red,"","","DarkRed")
hi! link Error ErrorMsg
hi! link MoreMsg Special
call s:X("Question","65C254","","","Green","")


" Spell Checking

call s:X("SpellBad","FF5252","302028","underline","Magenta","")
call s:X("SpellCap",amber,"","underline","","")
call s:X("SpellRare","","540063","underline","","DarkMagenta")
call s:X("SpellLocal","","2D7067","underline","","Green")

" Diff

hi! link diffRemoved Comment
hi! link diffAdded String

" VimDiff

call s:X("DiffAdd","D2EBBE","437019","","White","DarkGreen")
call s:X("DiffDelete","40000A","700009","","DarkRed","DarkRed")
call s:X("DiffChange","","2B5B77","","White","DarkBlue")
call s:X("DiffText",g:teal,"000000","reverse","Yellow","")

" PHP

hi! link phpFunctions Function
call s:X("StorageClass",yellow,"","","Red","")
hi! link phpSuperglobal Identifier
hi! link phpQuoteSingle StringDelimiter
hi! link phpQuoteDouble StringDelimiter
hi! link phpBoolean Constant
hi! link phpNull Constant
hi! link phpArrayPair Operator
hi! link phpOperator Normal
hi! link phpRelation Normal
hi! link phpVarSelector Identifier

" Python

hi! link pythonOperator Statement

" Ruby

hi! link rubySharpBang Constant
call s:X("rubyClass",g:purple,"","","DarkBlue","")
call s:X("rubyIdentifier",g:teal,"","","teal","")
call s:X("rubyPredefinedConstant", g:teal, "", "", "", "")
call s:X("rubyInclude",g:blue,"","","teal","")
call s:X("rubyInterpolation",g:teal,"","","teal","")
call s:X("rubyInterpolationDelimiter",g:blue_grey,"","","teal","")
call s:X("rubyConstant",g:amber,"","","teal","")
call s:X("rubyRailsFilterMethod",g:silver,"","","teal","")
call s:X("rubyRailsRenderMethod",g:silver,"","","teal","")
hi! link rubyFunction Function

call s:X("rubyInstanceVariable",g:red,"","","teal","")
call s:X("rubySymbol",g:teal,"","","Blue","")
hi! link rubyGlobalVariable rubyInstanceVariable
hi! link rubyModule rubyClass
call s:X("rubyControl",g:teal,"","","Blue","")
call s:X("rubyBlockParameterList", g:red, "", "", "", "")
call s:X("rubyBlockParameter", g:red, "", "", "", "")

hi! link rubyString String
hi! link rubyStringDelimiter StringDelimiter
" hi! link rubyStringDelimiter String
" hi! link rubyInterpolationDelimiter Identifier


call s:X("rubyRegexpDelimiter",g:blue_grey,"","","teal","")
call s:X("rubyRegexp",g:teal,"","","teal","")
call s:X("rubyRegexpSpecial",g:purple,"","","Magenta","")

call s:X("rubyPredefinedIdentifier",g:red,"","","Red","")

" Erlang

hi! link erlangAtom rubySymbol
hi! link erlangBIF rubyPredefinedIdentifier
hi! link erlangFunction rubyPredefinedIdentifier
hi! link erlangDirective Statement
hi! link erlangNode Identifier

" JavaScript

hi! link javaScriptValue Constant
hi! link javaScriptRegexpString rubyRegexp

" CoffeeScript

hi! link coffeeRegExp javaScriptRegexpString

" Lua

hi! link luaOperator Conditional

" C

hi! link cFormat Identifier
hi! link cOperator Constant

" Objective-C/Cocoa

hi! link objcClass Type
hi! link cocoaClass objcClass
hi! link objcSubclass objcClass
hi! link objcSuperclass objcClass
hi! link objcDirective rubyClass
hi! link objcStatement Constant
hi! link cocoaFunction Function
hi! link objcMethodName Identifier
hi! link objcMethodArg Normal
hi! link objcMessageName Identifier

" Vimscript

hi! link vimOper Normal

" HTML

hi! link htmlTag Statement
hi! link htmlEndTag htmlTag
hi! link htmlTagName htmlTag

" XML

hi! link xmlTag Statement
hi! link xmlEndTag xmlTag
hi! link xmlTagName xmlTag
hi! link xmlEqual xmlTag
hi! link xmlEntity Special
hi! link xmlEntityPunct xmlEntity
hi! link xmlDocTypeDecl PreProc
hi! link xmlDocTypeKeyword PreProc
hi! link xmlProcessingDelim xmlAttrib

" Startify

call s:X("StartifyBracket",g:mid_blue,"","","teal","")
call s:X("StartifySlash",g:mid_blue,"","","teal","")
call s:X("StartifySection",g:atom_blue,"","","teal","")
call s:X("StartifyHeader",g:atom_blue,"","","teal","")
call s:X("StartifyNumber",g:red,"","","teal","")
call s:X("StartifyFile",g:silver,"","","teal","")
call s:X("StartifyPath",g:blue_grey,"","","teal","")
call s:X("StartifySpecial",g:silver,"","","teal","")
call s:X("StartifyFooter",g:silver,"","","teal","")

" Debugger.vim

call s:X("DbgCurrent","DEEBFE","345FA8","","White","DarkBlue")
call s:X("DbgBreakPt","","4F0037","","","DarkMagenta")

" CtrlP

call s:X("CtrlPNoEntries",g:dark_blue,g:silver,"","White","DarkBlue")
call s:X("CtrlPMode1",g:dark_blue,g:silver,"italic","","White")
call s:X("CtrlPMode2",g:dark_blue,g:silver,"italic","","White")
call s:X("CtrlPPrtCursor",g:dark_blue,g:silver,"","White","DarkBlue")
call s:X("CtrlPStats",g:dark_blue,g:silver,"","White","DarkBlue")
call s:X("CtrlPMatch",g:dark_blue,g:silver,"","White","DarkBlue")

" vim-indent-guides

if !exists("g:indent_guides_auto_colors")
  let g:indent_guides_auto_colors = 0
endif
call s:X("IndentGuidesOdd","","232323","","","")
call s:X("IndentGuidesEven","","1b1b1b","","","")

" Plugins, etc.

hi! link TagListFileName Directory
call s:X("PreciseJumpTarget","B9ED67","405026","","White","Green")

if !exists("g:background_color_256")
  let g:background_color_256=233
end
" Manual overrides for 256-color terminals. Dark colors auto-map badly.
if !s:low_color
  hi StatusLineNC ctermbg=235
  hi Folded ctermbg=236
  hi FoldColumn ctermbg=234
  hi SignColumn ctermbg=236
  hi CursorColumn ctermbg=234
  hi CursorLine ctermbg=234
  hi SpecialKey ctermbg=234
  "exec "hi NonText ctermbg=".g:background_color_256
  hi Nontext ctermbg=none ctermfg=none
  "exec "hi LineNr ctermbg=".g:background_color_256
  hi LineNr ctermbg=none
  hi DiffText ctermfg=81
  "exec "hi Normal ctermbg=".g:background_color_256
  hi Normal ctermbg=none
  hi DbgBreakPt ctermbg=53
  hi IndentGuidesOdd ctermbg=235
  hi IndentGuidesEven ctermbg=234
endif

if exists("g:onedark_overrides")
  fun! s:load_colors(defs)
    for [l:group, l:v] in items(a:defs)
      call s:X(l:group, get(l:v, 'guifg', ''), get(l:v, 'guibg', ''),
      \                 get(l:v, 'attr', ''),
      \                 get(l:v, 'ctermfg', ''), get(l:v, 'ctermbg', ''))
      if !s:low_color
        for l:prop in ['ctermfg', 'ctermbg']
          let l:override_key = '256'.l:prop
          if has_key(l:v, l:override_key)
            exec "hi ".l:group." ".l:prop."=".l:v[l:override_key]
          endif
        endfor
      endif
      unlet l:group
      unlet l:v
    endfor
  endfun
  call s:load_colors(g:onedark_overrides)
  delf s:load_colors
endif

" delete functions {{{
delf s:X
delf s:rgb
delf s:color
delf s:rgb_color
delf s:rgb_level
delf s:rgb_number
delf s:grey_color
delf s:grey_level
delf s:grey_number
" }}}